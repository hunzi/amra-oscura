# Amra die Bardin

Ein Chatbot für Felix Pathfinder Kampagne in Discord. Soll wenn angesprochen, Geschichten der Spieler zum Besten geben.

## Kommandos

**Commands** beginnen mit "!". Sie sind dazu gedacht, einfach zu sein und eher im privaten Chat mit dem Bot genutzt zu werden.
**Mentions** werden eingeleitet, in dem man den Bot mit @ diret anspricht. Sie sollen fluff freundlich sein und im öffentlichen Chat genutzt werden können.

| Aktion                                         |  Command                                        | Mention                                                                          |
|------------------------------------------------|-------------------------------------------------|----------------------------------------------------------------------------------|
| Speichert *[Geschichte]* unter *[Name]*        | **!create** *[Name]* **!body** *[Geschichte]*   | **ich habe eine Geschichte für dich:** _[Name]_**! Sie geht so:** *[Geschichte]* |
| Lädt Geschichte *[Name]*                       | **!read** *[Name]*                              | **erzähl mir von** *[Name]*                                                      |
| Zeigt alle Amra bekannten Geschichten an       | **!list**                                       | **welche Geschichten kennst du?**                                                |
| Hängt *[Update]* an die Geschichte *[Name]* an | **!update** *[Name]* **!body** *[Update]*       |                                                                                  |
| Löscht die Geschichte *[Name]*                 | **!destroy** *[Name]* oder **!delete** *[Name]* |                                                                                  |
| Erzählt eine zufällige Geschichte.             | **!random**                                     | **erzähl mir eine Geschichte**                                                   |
