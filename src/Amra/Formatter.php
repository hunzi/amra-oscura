<?php

namespace Amra;

class Formatter {
    public function format($input) {
        if('array' === gettype($input)) {
            $string = "**" . $input['title'] . "**\n" . $input['body'];
        } else {
            $string = $input;
        }

        return $string;
    } 
}
