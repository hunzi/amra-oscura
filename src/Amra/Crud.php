<?php

namespace Amra;

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

class Crud
{
    protected $client;
    protected $cache;
    protected $spreadsheet_id;
    protected $table_name;
    protected $table_range;
    protected $table_full;
    protected $tokenPath;
    protected $clientIdPath;

    public function __construct($client = null)
    {
        $this->spreadsheet_id = defined("SHEET_ID") ? SHEET_ID : "";
        $this->table_name = defined("TABLE_NAME") ? TABLE_NAME : "";
        $this->table_range = defined("TABLE_RANGE") ? TABLE_RANGE : "";
        $this->table_full = $this->table_name . "!" . $this->table_range;
        $this->tokenPath;
        $this->clientIdPath;
        $this->cache = new \React\Cache\ArrayCache();
        try {
            $this->initClient($client);
        } catch (\Exception $e) { }
    }

    public function initClient($client = null)
    {
        if (isset($client) && null !== $client) {
            $this->client = $client;
        }
        $client = new \Google_Client();
        $client->setApplicationName('Google Sheets API PHP Quickstart');
        $client->setScopes(\Google_Service_Sheets::SPREADSHEETS);
        $client->setAuthConfig($this->clientIdPath);
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        // The file token.json stores the user's access and refresh tokens, and is
        // created automatically when the authorization flow completes for the first
        // time.
        if (file_exists($this->tokenPath)) {
            $accessToken = json_decode(file_get_contents($this->tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($client->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($this->tokenPath))) {
                mkdir(dirname($this->tokenPath), 0700, true);
            }
            file_put_contents($this->tokenPath, json_encode($client->getAccessToken()));
        }

        $this->client = $client;
    }

    public function create(string $name, string $body)
    {
        echo "\CREATE\n";
        $append = function () use ($name, $body) {
            $service = new \Google_Service_Sheets($this->client);

            $range = $this->table_name . '!A:B';

            $body = new \Google_Service_Sheets_ValueRange([
                'values' => [
                    [$name, $body]
                ]
            ]);

            $params = [
                'valueInputOption' => 'RAW'
            ];

            $response = $service->spreadsheets_values->append(
                $this->spreadsheet_id,
                $this->table_full,
                $body,
                $params
            );

            return $response;
        };
        return $this->cache->delete('data')->then($append);
    }

    public function read(string $storyname)
    {
        echo "READ\n";
        $find_story = function ($data) use ($storyname) {
            $story = false;
            for ($row_index = 0; $row_index < count($data); $row_index++) {
                $row_storyname = $data[$row_index][0];
                if ($storyname === $row_storyname) {
                    $story = $data[$row_index][1];
                    break;
                }
            }
            return $story;
        };
        return $this
            ->cache
            ->get('data')
            ->then($this->check_in_cache())
            ->then($find_story);
    }

    public function random()
    {
        echo "RANDOM\n";
        $random_story = function ($data) {
            $nr = rand(0, count($data) - 1);
            return "**" . $data[$nr][0] . "**\n\n" . $data[$nr][1];
        };
        return $this
            ->cache
            ->get('data')
            ->then($this->check_in_cache())
            ->then($random_story);
    }

    public function list()
    {
        echo "LIST\n";
        $list = function ($data) {
            $list = "";
            foreach ($data as $row) {
                $list .= "* " . $row[0] . "\n";
            }
            return $list;
        };
        return $this
            ->cache
            ->get('data')
            ->then($this->check_in_cache())
            ->then($list);
    }

    public function update(string $name, $update)
    {
        echo "\UPDATE\n";
        $update_story = function () use ($name, $update) {
            $updates = 0;
            $data = $this->get_data();
            foreach ($data as &$row) {
                $title = $row[0];
                $story = $row[1];
                if ($name === $title) {
                    $row[1] .= $update;
                    $updates++;
                    break;
                }
            }
            unset($row);
            if ($updates) {
                return $data;
            }
            return false;
        };

        $promise = \React\Promise\resolve("");
        return $promise->then($update_story)->then($this->update_sheet());
    }

    public function delete(string $name)
    {
        echo "\DELETE\n";
        $offset = false;
        $delete_story = function () use ($name) {
            $old_data = $this->get_data();
            $data = [];
            foreach ($old_data as $row) {
                if ($name === $row[0]) {
                    continue;
                }
                array_push($data, $row);
            }
            array_push($data, ["", ""]);
            return $data;
        };
        $promise = \React\Promise\resolve();
        return $promise
            ->then($delete_story)
            ->then($this->update_sheet());
    }

    public function get_data()
    {
        $service = new \Google_Service_Sheets($this->client);
        $range = $this->table_full;

        $response = $service->spreadsheets_values->get($this->spreadsheet_id, $range);
        $values = $response->getValues();

        $this->cache->set('data', $values, 60);
        return $values;
    }

    private function check_in_cache()
    {
        return function ($response) {
            if (empty($response)) {
                return $this->get_data();
            }
            return $response;
        };
    }

    private function update_sheet()
    {
        return function ($data) {
            if (!$data) {
                return false;
            }

            $service = new \Google_Service_Sheets($this->client);
            $id = $this->spreadsheet_id;
            $range = $this->table_full;
            $body = new \Google_Service_Sheets_ValueRange([
                'values' => $data
            ]);
            $params = [
                'valueInputOption' => 'RAW'
            ];

            $result = $service->spreadsheets_values->update(
                $id,
                $range,
                $body,
                $params
            );
            // Clear Cache
            return $this->cache->delete('data')->then(function () use ($result) {
                return $result;
            });
        };
    }

    public function setSpreadsheetId($id)
    {
        $this->spreadsheet_id = $id;
    }

    public function setTableName($name)
    {
        $this->table_name = $name;
        $this->table_full = "$this->table_name!$this->table_range";
    }

    public function setTableRange($range)
    {
        $this->table_range = $range;
        $this->table_full = "$this->table_name!$this->table_range";
    }

    public function setTokenPath($path)
    {
        $this->tokenPath = $path;
    }

    public function setClientIdPath($path)
    {
        $this->clientIdPath = $path;
    }
}
