<?php

namespace Amra;

class Amra
{
    private $parser;
    private $formatter;
    private $crud;

    public function __construct()
    {
        $this->parser = new Parser\Parser;
        $this->formatter = new Formatter;
        try {
            $this->crud = new Crud;
        } catch (\Exception $e) { }
    }

    public function take_request($message)
    {
        $parsed = $this->parser->parse($message->content);

        switch ($parsed->action) {
            case "CREATE":
                $response = $this->crud->create($parsed->title, $parsed->body);
                break;
            case "READ":
                $response = $this->crud->read($parsed->title);
                break;
            case "CATALOGUE":
                $response = $this->crud->list();
                break;
            case "RANDOM":
                $response = $this->crud->random();
                break;
            case "UPDATE":
                $response = $this->crud->update($parsed->title, $parsed->body);
                break;
            case "DESTROY":
                $response = $this->crud->delete($parsed->title);
                break;
            case "ERROR":
                $response = \React\Promise\resolve(null);
                break;
            default:
                return;
        }

        return $response->then(function ($data) use ($parsed) {
            if (empty($data)) {
                $answer = $parsed->error;
            } elseif (in_array(gettype($data), ['string', 'array'])) {
                $answer = $this->formatter->format($data);
            } else {
                $answer = $parsed->success;
            }
            return $answer;
        });
    }

    public function initParser($parser)
    {
        $this->parser = $parser;
    }

    public function initCrud($crud)
    {
        $this->crud = $crud;
    }

    public function initFormatter($formatter)
    {
        $this->formatter = $formatter;
    }

    public function set_mentions(array $mentions)
    {
        $this->parser->set_mentions($mentions);
    }
}
