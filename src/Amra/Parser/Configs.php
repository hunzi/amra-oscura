<?php

namespace Amra\Parser;

class Configs {
    public static function create(string $title, string $body) {
        $parsed = new Parsed;
        $parsed->action = "CREATE";
        $parsed->success = "Ich habe mir die Geschichte gemerkt!";
        $parsed->error = "Ich konnte mir das nicht merken.";
        $parsed->title = $title;
        $parsed->body = $body;
        return $parsed;
    }

    public static function read(string $title) {
        $parsed = new Parsed;
        $parsed->action = "READ";
        $parsed->error = "Ich kenne diese Geschichte nicht.";
        $parsed->title = $title;
        return $parsed;
    }

    public static function random() {
        $parsed = new Parsed;
        $parsed->action = "RANDOM";
        $parsed->error = "Ich erinnere mich an keine Geschichte.";
        return $parsed;
    }

    public static function catalogue() {
        $parsed = new Parsed;
        $parsed->action = "CATALOGUE";
        $parsed->error = "Ich erinnere mich an keine Geschichte...";
        return $parsed;
    }

    public static function update(string $title, string $update) {
        $parsed = new Parsed;
        $parsed->action = "UPDATE";
        $parsed->success = "Das habe ich mir gemerkt!";
        $parsed->error = "Ich konnte mir das nicht merken.";
        $parsed->title = $title;
        $parsed->body = $update;
        return $parsed;
    }

    public static function delete(string $title) {
        $parsed = new Parsed;
        $parsed->action = "DESTROY";
        $parsed->success = "";
        $parsed->error = "";
        $parsed->title = $title;
        return $parsed;
    }

    public static function error(string $message = "Unbekanntes Kommando.") {
        $parsed = new Parsed;
        $parsed->action = "ERROR";
        $parsed->success = $message;
        $parsed->error = $message;
        return $parsed;
    }

    public static function nothing() {
        $parsed = new Parsed;
        $parsed->action = "NOTHING";
        return $parsed;
    }
}