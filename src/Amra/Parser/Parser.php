<?php

namespace Amra\Parser;

use Amra\Parser\Parsed;

class Parser
{
    protected $mentions = [];
    protected $read_regex = '/.*Geschichte für dich: /i';
    protected $read_regex_args = '/(?<=Geschichte für dich: ).*?(?=\!)|(?<=Sie geht so: ).*$/i';

    public function parse(string $message): Parsed
    {
        if ($this->is_mention($message)) {
            return $this->parse_mention($message);
        } elseif (preg_match('/^\!/', $message)) {
            return $this->parse_command($message);
        }
        return Configs::nothing();
    }

    private function parse_command(string $message): Parsed
    {
        $message_parts = explode(" ", $message);
        switch (array_shift($message_parts)) {
            case "!create":
                $story = $this->seperate_arguments($message_parts, "title");
                if (empty($story->title) || empty($story->body)) {
                    return Configs::error("Fehlende Argumente");
                }
                return Configs::create($story->title, $story->body);
            case "!read":
                return Configs::read(implode(" ", $message_parts));
            case "!list":
                return Configs::catalogue();
            case "!random":
                return Configs::random();
            case "!update":
                $story = $this->seperate_arguments($message_parts, "title");
                if (empty($story->title) || empty($story->body)) {
                    return Configs::error("Fehlende Argumente");
                }
                return Configs::update($story->title, $story->body);
            case "!delete":
            case "!destroy":
                return Configs::delete(implode(" ", $message_parts));
            default:
                return Configs::error("Unbekanntes Kommando.");
        }
    }

    private function parse_mention(string $content): Parsed
    {
        if (preg_match($this->read_regex, $content)) {
            preg_match_all($this->read_regex_args, $content, $args);
            return Configs::create($args[0][0], $args[0][1]);
        } elseif (preg_match('/.*erzähl(.*)von /i', $content)) {
            preg_match('/(?<=erzähl (mir |uns )von ).*/i', $content, $storynames);
            return Configs::read($storynames[0]);
        } elseif (preg_match('/.*welche Geschichte(.*)\?/i', $content)) {
            return Configs::catalogue();
        } elseif (preg_match('/NOPE/', $content)) {
            return Configs::update($title, $body);
        } elseif (preg_match('/NOPE/', $content)) {
            return Configs::delete($title);
        } elseif (preg_match('/.*erzähl(.*)Geschichte?/i', $content)) {
            return Configs::random();
        } else {
            return Configs::error('Dazu fällt mir nichts ein...');
        }
    }

    private function seperate_arguments(array $string_parts, string $firstkey = "title"): \stdClass
    {
        $return = [];
        $words = [];
        $key = $firstkey;
        foreach ($string_parts as $word) {
            if (preg_match("/^\!/", $word)) {
                $return[$key] = implode(" ", $words);
                $key = str_replace("!", "", $word);
                $words = [];
            } else {
                $words[] = $word;
            }
        }
        $return[$key] = implode(" ", $words);
        return (object)$return;
    }

    public function set_mentions(array $mentions)
    {
        $this->mentions = $mentions;
    }

    private function is_mention(string $message): bool
    {
        if (4 > count($this->mentions)) return false;
        $start = \trim(\substr($message, 0, $this->mentions[3]));
        return $start === $this->mentions[0] || $start === $this->mentions[2];
    }
}
