<?php

namespace Amra\Parser;

class Parsed
{
    public $error = '';
    public $success = '';
    public $action = '';
    private $data = [];

    public function __set(string $key, string $data)
    {
        if (empty($this->data[$key])) {
            $this->data[$key] = $data;
        }
    }

    public function __get(string $key)
    {
        if (isset($this->data[$key]))
            return $this->data[$key];
        return new \Error("Tried to access unknown key");
    }
}

