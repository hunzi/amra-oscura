<?php

define("BASEPATH", dirname(__DIR__));
define("LIBPATH", BASEPATH . "/src/Amra");
define("CREDENTIALS", BASEPATH . "/credentials");

require_once BASEPATH . "/config.php";
require_once BASEPATH . "/vendor/autoload.php";

$loop = \React\EventLoop\Factory::create();
$amra = new \Amra\Amra();
$client = new \CharlotteDunois\Yasmin\Client(array(
    'ws.disabledEvents' => [
        'TYPING_START'
    ]
), $loop);

$mentions = [];

$client->once('ready', function () use ($client, &$mentions) {
    $format1 = '<@' . $client->user->id . '>';
    $format2 = '<@!' . $client->user->id . '>';
    
    $mentions = [
        $format1,
        strlen($format1),
        $format2,
        strlen($format2)
    ];
    echo BOT_NAME, " ready for action!\n";
});

$client->on('message', function ($message) use ($client, &$mentions, $amra) {
    try {
        $answer = $amra->take_request($message, $mentions);
        $answer->done(function ($answer) use ($message) {
            if(empty($answer)) return;
            $message->send($answer)->done(null, function ($error) {
                echo $error . PHP_EOL;
            });
        });
    } catch (\Exception $error) {
        echo $error;
    }
});

$client->login(SECRET);

echo "Ready to start ", BOT_NAME, "...\n";
$loop->run();

