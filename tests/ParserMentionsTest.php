<?php 

namespace tests;

use PHPUnit\Framework\TestCase;
use Amra\Parser\Parser;

class ParserMentionsTest extends ParserTestCase
{
    public function testParseCreate()
    {
        $sentences = [
            "$this->mention_format1 ich habe eine Geschichte für dich: $this->title! Sie geht so: $this->body",
            "$this->mention_format2 ich habe eine Geschichte für dich: $this->title! Sie geht so: $this->body",
        ];

        foreach ($sentences as $sentence) {
            $response = $this->parser->parse($sentence);
            
            $this->assertEquals("CREATE", $response->action);
            $this->assertEquals($this->title, $response->title);
            $this->assertEquals($this->body, $response->body);
        }
    }

    public function testParseRead()
    {
        $sentences = [
            "$this->mention_format1 erzähl mir von $this->title",
            "$this->mention_format2 erzähl mir von $this->title",
        ];

        foreach ($sentences as $sentence) {
            $response = $this->parser->parse($sentence);
            
            $this->assertEquals("READ", $response->action);
            $this->assertEquals($this->title, $response->title);
        }


    }

    public function testParseCatalogue()
    {
        $sentences = [
            "$this->mention_format1 welche Geschichten kennst du?",
            "$this->mention_format2 welche Geschichten kennst du?",
        ];

        foreach ($sentences as $sentence) {
            $response = $this->parser->parse($sentence);
            
            $this->assertEquals("CATALOGUE", $response->action);
        }
    }

    public function testParseRandom()
    {
        $sentences = [
            "$this->mention_format1 erzähl mir eine Geschichte",
            "$this->mention_format2 erzähl mir eine Geschichte",
        ];

        foreach ($sentences as $sentence) {
            $response = $this->parser->parse($sentence);
            $this->assertEquals("RANDOM", $response->action, "Error when parsing '$sentence'");
        }


    }

    public function testParseUpdate()
    {
        $sentences = [];

        foreach($sentences as $sentence) {
            $response = $this->parser->parse($sentence);
    
            $this->assertEquals("UPDATE", $response->action);
            $this->assertEquals($this->title, $response->title);
            $this->assertEquals($this->body, $response->body);
        }
    }

    public function testParseDelete()
    {
        $sentences = [];

        foreach($sentences as $sentence) { 
            $response = $this->parser->parse($sentence);
    
            $this->assertEquals("DESTROY", $response->action);
            $this->assertEquals($this->title, $response->title);
        }
    }

    public function testParseError()
    {
        $sentences = [
            "$this->mention_format1 blaberblubb",
            "$this->mention_format2 blaberblubb",
        ];

        foreach($sentences as $sentence) {
            $response = $this->parser->parse($sentence);
    
            $this->assertEquals("ERROR", $response->action);
        }
    }
}