<?php

namespace tests;

use PHPUnit\Framework\TestCase;
use Amra\Parser\Parser;

class ParserCommandsTest extends ParserTestCase
{
    public function testParseCreate()
    {
        $sentence = "!create $this->title !body $this->body";
        $response = $this->parser->parse($sentence, $this->mentions);

        $this->assertEquals("CREATE", $response->action);
        $this->assertEquals($this->title, $response->title);
        $this->assertEquals($this->body, $response->body);
    }

    public function testParseRead()
    {
        $sentence = "!read $this->title";
        $response = $this->parser->parse($sentence, $this->mentions);

        $this->assertEquals("READ", $response->action);
        $this->assertEquals($this->title, $response->title);
    }

    public function testParseCatalogue()
    {
        $sentence = "!list";
        $response = $this->parser->parse($sentence, $this->mentions);

        $this->assertEquals("CATALOGUE", $response->action);
    }

    public function testParseRandom()
    {
        $sentence = "!random";
        $response = $this->parser->parse($sentence, $this->mentions);

        $this->assertEquals("RANDOM", $response->action);
    }

    public function testParseUpdate()
    {
        $request = "!update $this->title !body $this->body";
        $response = $this->parser->parse($request, $this->mentions);

        $this->assertEquals("UPDATE", $response->action);
        $this->assertEquals($this->title, $response->title);
        $this->assertEquals($this->body, $response->body);
    }

    public function testParseDelete()
    {
        $request = "!delete $this->title";
        $response = $this->parser->parse($request, $this->mentions);

        $this->assertEquals("DESTROY", $response->action);
        $this->assertEquals($this->title, $response->title);
    }

    public function testParseError()
    {
        $request = "!errror $this->title !body $this->body";
        $response = $this->parser->parse($request, $this->mentions);

        $this->assertEquals("ERROR", $response->action);
    }
}
