<?php

use \Amra\Amra;
use \PHPUnit\Framework\TestCase;
use \Amra\Parser\Configs;

class AmraTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->obj = new class
        {
            public $content = "";
        };
    }

    public function test_take_request_create()
    {
        $expected = "create";
        $amra = new Amra();
        $amra->initParser(new MockParser(Configs::create("", "")));
        $amra->initCrud(new MockCrud($expected));
        $amra->initFormatter(new MockFormatter);

        $response = $amra->take_request($this->obj);

        $response->done(function (string $answer) use ($expected) {
            $this->assertEquals($expected, $answer);
        });
    }

    public function test_take_request_read()
    {
        $expected = "read";
        $amra = new Amra();
        $amra->initParser(new MockParser(Configs::read("")));
        $amra->initCrud(new MockCrud);
        $amra->initFormatter(new MockFormatter);

        $response = $amra->take_request($this->obj);

        $response->done(function (string $answer) use ($expected) {
            $this->assertEquals($expected, $answer);
        });
    }

    public function test_take_request_list()
    {
        $expected = "list";
        $amra = new Amra();
        $amra->initParser(new MockParser(Configs::catalogue()));
        $amra->initCrud(new MockCrud);
        $amra->initFormatter(new MockFormatter);

        $response = $amra->take_request($this->obj);

        $response->done(function (string $answer) use ($expected) {
            $this->assertEquals($expected, $answer);
        });
    }

    public function test_take_request_random()
    {
        $expected = "random";
        $amra = new Amra();
        $amra->initParser(new MockParser(Configs::random()));
        $amra->initCrud(new MockCrud());
        $amra->initFormatter(new MockFormatter);

        $response = $amra->take_request($this->obj);

        $response->done(function (string $answer) use ($expected) {
            $this->assertEquals($expected, $answer);
        });
    }

    public function test_take_request_update()
    {
        $expected = "update";
        $amra = new Amra();
        $amra->initParser(new MockParser(Configs::update("", "")));
        $amra->initCrud(new MockCrud());
        $amra->initFormatter(new MockFormatter);

        $response = $amra->take_request($this->obj);

        $response->done(function (string $answer) use ($expected) {
            $this->assertEquals($expected, $answer);
        });
    }

    public function test_take_request_delete()
    {
        $expected = "delete";
        $amra = new Amra();
        $amra->initParser(new MockParser(Configs::delete("")));
        $amra->initCrud(new MockCrud());
        $amra->initFormatter(new MockFormatter);

        $response = $amra->take_request($this->obj);

        $response->done(function (string $answer) use ($expected) {
            $this->assertEquals($expected, $answer);
        });
    }
}

class MockFormatter
{
    public function __call($name_, $args)
    {
        return $args[0];
    }
}

class MockCrud
{
    public function __call($name, $args_)
    {
        return \React\Promise\resolve($name);
    }
}

class MockParser
{
    public function __construct($answer)
    {
        $this->answer = $answer;
    }

    public function setAnswer($answer)
    {
        $this->answer = $answer;
    }

    public function parse($input_)
    {
        return $this->answer;
    }
}
