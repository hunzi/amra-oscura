<?php

namespace tests;

use PHPUnit\Framework\TestCase;
use Amra\Parser\Parser;

class ParserTestCase extends TestCase {
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->parser = new Parser;
        $this->client_id = '12345678910';
        $this->mention_format1 = '<@' . $this->client_id . '>';
        $this->mention_format2 = '<@!' . $this->client_id . '>';
        $this->mentions = [
            $this->mention_format1,
            strlen($this->mention_format1),
            $this->mention_format2,
            strlen($this->mention_format2)
        ];
        $this->title = "MyTitle";
        $this->body = "MyBody";

        $this->parser->set_mentions($this->mentions);
    }
}