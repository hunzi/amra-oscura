<?php

namespace tests;

use Amra\Crud;
use PHPUnit\Framework\TestCase;

class CrudTest extends TestCase
{
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->crud = new Crud;
        $this->crud->setSpreadsheetId('1qpRsuWb7nr5EC_VgimnCXH10XUbZpmpfeEXZj5wz8Bg');
        $this->crud->setTableName('MyTestTable');
        $this->crud->setTableRange('A2:B');
        $this->crud->setClientIdPath(dirname(__DIR__) . "/credentials/client_id.json");
        $this->crud->setTokenPath(dirname(__DIR__) . "/credentials/token.json");
        $this->crud->initClient();
    }

    public function test_create()
    {
        $data = $this->crud->get_data();
        $entries_count = count($data);
        $entry = (object) [
            "title" => "MyTitle",
            "body" => "MyBody"
        ];
        $response = $this->crud->create($entry->title, $entry->body);
        $data = $this->crud->get_data();
        $last_entry = $data[count($data)-1];
        $this->assertEquals($entries_count+1, count($data));
        $this->assertEquals($entry->title, $last_entry[0]);
        $this->assertEquals($entry->body, $last_entry[1]);
    }

    public function test_update()
    {
        $data = $this->crud->get_data();
        $last_entry = $data[count($data)-1];
        $update = "TESTME";
        $this->crud->update($last_entry[0], $update);

        $data = $this->crud->get_data();
        foreach ($data as $row) {
            if ($last_entry[0] === $row[0]) {
                $new_last_entry = $row;
                break;
            }
        }
        $this->assertNotEmpty($new_last_entry);
        $this->assertEquals($last_entry[1].$update, $new_last_entry[1]);
    }

    public function test_read() {
        $data = $this->crud->get_data();
        foreach($data as $story) {
            $response = $this->crud->read($story[0]);
            $response->done(function($answer) use ($story) {
                $this->assertEquals($story[1], $answer);
            });
        }
    }

    public function test_list() {
        $data = $this->crud->get_data();
        $entry_number = count($data);

        $response = $this->crud->list();

        $response->done(function($list) use ($entry_number) {
            $hmm = explode("\n", $list);
            $this->assertEquals($entry_number, count($hmm)-1);
        });
    }

    public function test_random() {
        $i = 0;
        $lastBody = "";
        $changed = false;
        while($i++ < 100) {
            $response = $this->crud->random();
            $response->done(function($body) use ($i, &$lastBody, &$changed) {
                if((1 >= $i) && ($lastBody !== $body)) {
                    $changed = true;
                }
                $lastBody = $body;
            });
            if($changed) break;
        }
        $this->assertTrue($changed);
    }

    public function test_delete()
    {
        $data = $this->crud->get_data();
        $entry_number = count($data);
        $last_entry = $data[$entry_number-1];

        $this->crud->delete($last_entry[0]);

        $data = $this->crud->get_data();
        $new_entry_number = count($data);
        $new_last_entry = $data[$new_entry_number-1];

        $this->assertEquals($entry_number-1, $new_entry_number);
        $this->assertNotEquals($last_entry[0], $new_last_entry[0]);
    }
}
