FROM alpine/git as clone
WORKDIR /data
RUN git clone https://gitea.mangyjoe.de/destructed3/AmraTheBard.git .

FROM composer as install
WORKDIR /data
COPY --from=clone /data .
RUN composer install --ignore-platform-reqs --no-scripts --no-progress --no-suggest --classmap-authoritative

FROM php:cli-alpine
ENV CODE /code
RUN apk add $PHPIZE_DEPS
RUN pecl install eio && docker-php-ext-enable eio
RUN docker-php-ext-install bcmath
COPY --from=install /data $CODE
WORKDIR $CODE
CMD ["scripts/startBot"]
